angular.module('app', ['ui.bootstrap', 'ui.router'])
.config(function($stateProvider, $urlRouterProvider) {
  //
  // For any unmatched url, redirect to /state1
  $urlRouterProvider.otherwise("/users");
  //
  // Now set up the states
  $stateProvider
    
    .state('users', {
      url: "/users",
      views: {
        'main-view': {
          templateUrl: "home.html",
          controller: "HomeCtrl"
        }
      }
    })
    .state('user', {
      url: "/user/:userId",
      views: {
        'main-view': {
          templateUrl: "user.html",
          controller: "UserCtrl"
        }
      }
    })
})

.controller('UserCtrl', function($scope, $stateParams, $http){
  console.log("User: "+$stateParams.userId);

  $scope.user = {};

  $http.get('/users/'+$stateParams.userId).success(function(data){
    $scope.user = data;
  });

  $scope.editUser = function() {
    console.log("Update user");
    $http.put("/users/"+$stateParams.userId+"?name="+$scope.user.name+'&email='+$scope.user.email).success(function(){
      console.log("User updated");
    });
  }
})

.controller('HomeCtrl', function($scope, $http){
  console.log("Hey");
  $scope.users = [];

  

  $scope.refresh = function() {
    $http.get('/users').success(function(data){
      $scope.users=data;
    });
  };



  $scope.refresh();

  $scope.deleteUser = function(id) {
    $http.delete('/users/'+id).success(function(data){
      $scope.refresh();
    });
  };

  $scope.addUser = function() {
    console.log("New User: "+$scope.newUser)
    $http.post("/users?name="+$scope.newUser+'&email='+$scope.newUserMail).success(function(){
      console.log("User added");
      $scope.newUser = "";
      $scope.newUserMail = "";
      $scope.refresh();

    });
  }
})

.run(function($rootScope, $state){
  console.log("Starting app")
});