require 'rubygems'
require 'bundler/setup'

# # Does not work with grape
# require 'rack/contrib/try_static'
# use Rack::TryStatic,
#   :root => File.expand_path('../assets', __FILE__),
#   :urls => %w[/], :try => ['.html', 'index.html', '/index.html']

require 'pathname'

REST_EXAMPLE_ROOT=Pathname.new Dir.pwd

require REST_EXAMPLE_ROOT.join("lib/rest-example")



run REST::API