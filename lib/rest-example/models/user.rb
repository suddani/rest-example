class User
  def self.create! opt
    id = next_id
    self.users << User.new(id, opt[:name], opt[:email])
    self.users[-1]
  end
  def self.find id
    self.users.select {|user|
      user.id.to_i == id.to_i
    } [0]
  end
  def self.delete! id
    @users = self.users.select {|user|
      user.id.to_i != id.to_i
    }
  end

  def self.users
    @users||=[User.new(0, "robin", "robin@test.de"),User.new(1, "test", "test@test.de")]
  end

  def self.all
    self.users
  end

  def self.next_id
    @next_id||=1
    @next_id+=1
  end



  attr_accessor :id, :name, :email
  def initialize(id, name, email)
    @id = id
    @name = name
    @email = email||"standard@mail.de"
  end

  def to_json a=nil
    {
      :id => id,
      :name => name,
      :email => email
    }.to_json
  end
end