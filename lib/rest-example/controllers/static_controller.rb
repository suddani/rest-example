class StaticController
  attr_reader :params, :env

  def initialize(router, params, env)
    @router = router
    @params = params
    @env = env
  end

  def js resource
    format "application/javascript"
    load "assets/js/#{resource}"
  end

  def css resource
    format "text/css"
    load "assets/css/#{resource}"
  end

  def html resource
    format "text/html"
    load "assets/#{resource}"
  end

  def font resource
    format "font/opentype"
    load "assets/fonts/#{resource}"
  end

  def load resource
    render File.read("#{resource}")
    nil
  end

  private
    def format(f)
      @router.content_type f
    end
    def render(f)
      @router.body f
    end
end