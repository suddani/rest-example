require 'grape'
require 'json'

require './lib/rest-example/models/user'
require './lib/rest-example/controllers/static_controller'

module REST
  class API < Grape::API
    format :json
    default_format :json
    content_type :html, "text/html"
    content_type :txt, "text/plain"
    content_type :webp, "image/webp"
    content_type :image, "image/*"
    content_type :png, "image/png"
    content_type :jpeg, "image/jpeg"
    content_type :json, "application/json"
    content_type :js, "application/javascript"
    content_type :css, "text/css"
    content_type :xml, "text/xml"
    content_type :font, "font/opentype"

    formatter :webp, lambda { |object, env| object }
    formatter :png, lambda { |object, env| object }
    formatter :jpeg, lambda { |object, env| object }
    formatter :image, lambda { |object, env| object }
    formatter :css, lambda { |object, env| object.to_s }
    formatter :js, lambda { |object, env| object.to_s }
    formatter :html, lambda { |object, env| object.to_s }
    formatter :font, lambda { |object, env| object.to_s }

    resource :users do

      get do
        User.all
      end

      desc "Return a user."
      params do
        requires :id, type: Integer, desc: "User id."
      end
      route_param :id do
        get do
          User.find(params[:id])
        end
      end

      desc "Create a user."
      params do
        requires :name, type: String, desc: "Your user."
        requires :email, type: String, desc: "Your user mail."
      end
      post do
        User.create!({
          :name => params[:name],
          :email => params[:email]
        })
      end

      desc "Update a user."
      params do
        requires :id, type: String, desc: "Status ID."
        requires :name, type: String, desc: "Your user."
        requires :email, type: String, desc: "Your user mail."
      end
      put ':id' do
        User.find(params[:id]).name = params[:name]
        User.find(params[:id]).email = params[:email]
        User.find(params[:id])
      end

      desc "Delete a status."
      params do
        requires :id, type: String, desc: "User ID."
      end
      delete ':id' do
        User.delete!(params[:id])
      end
    end

    get '/js/*resource', anchor: false do
      StaticController.new(self, params,env).js params["resource"]
    end
    get '/css/*resource', anchor: false do
      StaticController.new(self, params,env).css params["resource"]
    end
    get '/*resource', anchor: false do
      StaticController.new(self, params,env).html(params["resource"])
    end
    get '/', anchor: false do
      StaticController.new(self, params,env).html "index.html"
    end
    get '/fonts/*resource', anchor: false do
      StaticController.new(self, params,env).font params["resource"]
    end
  end
end