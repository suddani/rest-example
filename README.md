# README #

Install ruby and bundler

```
#!bash

  bundle install
```

run server


```
#!bash

  bundle exec rackup
```


go to


```
#!ruby

  http://localhost:9292/html/index.html
```


in your browser